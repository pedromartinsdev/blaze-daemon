#ifndef __TCPSOCKETSTACK_H__
#define __TCPSOCKETSTACK_H__


#include "Xstack.h"
#include "TCPSocket.h"

namespace Network
{
	struct SocketObj
	{
		sockaddr_in		m_sadr;
		TCPSocket		*m_pSocket;
	};

	class TCPSocketStack : public Xstack < SocketObj >
	{
	public:
		SocketObj pop();
		void push(SocketObj pObj);
	};
}

#endif /* __TCPSOCKETSTACK_H__ */