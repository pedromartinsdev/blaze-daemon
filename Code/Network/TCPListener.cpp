#include "Main.h"

#include "Xmutex.h"
#include "TCPSocket.h"
#include "TCPSocketStack.h"
#include "TCPListener.h"


namespace Network
{
	TCPListener* TCPListener::m_pInstance;

	TCPListener::TCPListener(int port)
		: m_listenPort(port)
	{
		m_pMutex = new Xmutex();
		m_pSocket = new TCPSocket();
		m_pSocketPool = new TCPSocketStack();

		m_numConnections = 0;
		m_totalBytesReceived = 0;

		m_isListening = false;
		m_pInstance = this;
	}

	TCPListener::~TCPListener()
	{
		m_isListening = false;

		delete m_pSocket;
		delete m_pMutex;
		delete m_pSocketPool;
	}

	bool TCPListener::start()
	{
		if (m_pSocket->isValid())
		{
			FB_LOGINFO("Starting TCP listener on port %i.", m_listenPort);

			sockaddr_in sadr;
			memset(&sadr, 0, sizeof(struct sockaddr_in));

			// Create sockaddr
			sadr.sin_addr.s_addr = INADDR_ANY;
			sadr.sin_port = htons(m_listenPort);
			sadr.sin_family = AF_INET;

			// Bind
			if (m_pSocket->bind((sockaddr*)&sadr, sizeof(sadr)) != SOCKET_ERROR)
			{
				//FB_LOGINFO("Binding listen socket.");

				// Listen
				if (m_pSocket->listen(SOMAXCONN) != SOCKET_ERROR)
				{
					//FB_LOGINFO("Listening socket.");

					m_isListening = true;

					// Start receiving new connections
					_beginthread(TCPListener::processAccept, 0, NULL);


				}
				else
				{
					FB_LOGERROR("Failed to listen on port %i  (h_errno = %i).", m_listenPort, h_errno);
					return false;
				}
			}
			else
			{
				FB_LOGERROR("Failed to bind socket! (h_errno = %i)", h_errno);
				return false;
			}

			return true;
		}

		FB_LOGERROR("TCP listener failed to listen on port %i because socket is invalid (%i).",
			m_listenPort, h_errno);

		return false;
	}

	void TCPListener::processAccept(void* pArg)
	{
		// Lock the mutex
		m_pInstance->m_pMutex->lock();

		TCPSocket* pSocket = m_pInstance->m_pSocket;

		sockaddr_in sadr;
		int addrlen = sizeof(struct sockaddr_in);

		// Accept new connections while the server's listening
		while (m_pInstance->m_isListening)
		{
			memset(&sadr, 0, sizeof(struct sockaddr_in));

			TCPSocket* pConn = pSocket->accept((sockaddr*)&sadr, &addrlen);

			if (pConn->isValid())
			{
				// Push new connection to our socket pool
				m_pInstance->m_pSocketPool->push({ sadr, pConn });

				// Increment number of active connections
				InterlockedIncrement(&m_pInstance->m_numConnections);

				FB_LOGINFO("Accepted new connection from %s:%i.",
					inet_ntoa(sadr.sin_addr), htons(sadr.sin_port));

// 				char conn[4];
// 				sprintf(conn, "%i", m_pInstance->m_numConnections);
// 
// 				fb::StatusBox::AddItem(0, 0, "Number of connections", conn);

				// Start receiving data from our new connection
				_beginthread(TCPListener::processReceive, 0, NULL);
			}
			else
			{
				FB_LOGWARNING("Received invalid socket connection.");
			}
		}

		// Unlock the mutex when we're done
		m_pInstance->m_pMutex->unlock();
	}

	void TCPListener::processReceive(void* pArg)
	{
		SocketObj obj = m_pInstance->m_pSocketPool->pop();

		char buf[131072];

		// Receive data...
		int recvlen = obj.m_pSocket->recv(buf, sizeof(buf), 0);

		// ... while server is listening and everything else needed is correct
		while (m_pInstance->m_isListening && obj.m_pSocket->isValid() && recvlen != SOCKET_ERROR)
		{
			// Process the received data
			if (recvlen > 0)
			{
				buf[recvlen] = '\0';
				
				m_pInstance->m_totalBytesReceived += recvlen;

				if (m_pInstance->m_ReceivedDataCB)
					m_pInstance->m_ReceivedDataCB(obj, buf, recvlen);
			}
			else if (!recvlen) break;

			recvlen = obj.m_pSocket->recv(buf, sizeof(buf), 0);
		}

		// Client disconnected, decrement number of active connections
		InterlockedDecrement(&m_pInstance->m_numConnections);
	}
}