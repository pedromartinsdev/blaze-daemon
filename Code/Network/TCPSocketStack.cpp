#include "Main.h"
#include "TCPSocketStack.h"

namespace Network
{
	SocketObj TCPSocketStack::pop()
	{
		FB_LOGINFO("Popping socket object from the socket stack.");
		return Xstack::pop();
	}

	void TCPSocketStack::push(SocketObj pObj)
	{
		FB_LOGINFO("Pushing socket object onto the top of the socket stack.");
		return Xstack::push(pObj);
	}
}