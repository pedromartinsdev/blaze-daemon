#ifndef __TCPLISTENER_H__
#define __TCPLISTENER_H__

#include "Xmutex.h"
#include "TCPSocket.h"
#include "TCPSocketStack.h"

namespace Network
{
	typedef void(*ReceivedDataCB_t)(SocketObj obj, char* data, int length);

	class TCPListener
	{
	private:
		int m_listenPort;

		TCPSocket		*m_pSocket;
		Xmutex			*m_pMutex;
		TCPSocketStack	*m_pSocketPool;

		static TCPListener	*m_pInstance;


	public:
		long m_numConnections;
		uint m_totalBytesReceived;
		bool m_isListening;

		ReceivedDataCB_t m_ReceivedDataCB;

		TCPListener(int port);
		~TCPListener();

		bool start();

		static void processAccept(void* pArg);
		static void processReceive(void* pArg);
	};
}

#endif /* __TCPLISTENER_H__ */