#ifndef __TCPSOCKET_H__
#define __TCPSOCKET_H__

namespace Network
{
	class TCPSocket
	{
	private:
		int m_socketId;
		SOCKET m_socket;

	public:
		TCPSocket();
		TCPSocket(SOCKET sock);
		~TCPSocket();

		bool isValid();

		int bind(const sockaddr* name, int namelen);
		int listen(int backlog);

		TCPSocket* accept(sockaddr* addr, int* addrlen);

		int connect(sockaddr* name, int namelen);
		int recv(char* buf, int len, int flags);
		int send(const char* buf, int len, int flags);
	};
}

#endif /* __TCPSOCKET_H__ */