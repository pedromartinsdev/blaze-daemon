#include "Main.h"
#include "TCPSocketStack.h"
#include "TCPSocket.h"
#include "TCPListener.h"
#include "TCPServer.h"

namespace Network
{
	TCPServer* TCPServer::m_pInstance;

	TCPServer::TCPServer(int port)
	{
		m_pListener = new TCPListener(port);
		m_pListener->m_ReceivedDataCB = Network::TCPServer::processData;

		m_pInstance = this;
	}

	bool TCPServer::start()
	{
		return m_pListener->start();
	}

	void TCPServer::processData(Network::SocketObj from, char* data, int length)
	{
		m_pInstance->handleData(from, data, length);
	}
}