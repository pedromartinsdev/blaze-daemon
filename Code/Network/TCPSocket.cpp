#include "Main.h"
#include "TCPSocket.h"

namespace Network
{
	int g_socketId = 0;

	TCPSocket::TCPSocket()
	{
		m_socketId = g_socketId++;
		FB_LOGINFO("Opening TCP/IP socket with ID %i.", m_socketId);

		m_socket = socket(AF_INET, SOCK_STREAM, 0);

		if (!m_socket)
		{
			FB_LOGERROR("socket() failed - %i.", h_errno);
		}
	}

	TCPSocket::TCPSocket(SOCKET sock)
	{
		if (sock == INVALID_SOCKET)
		{
			FB_LOGERROR("Cannot use existing socket object that is invalid.");
		}
		else
		{
			m_socketId = g_socketId++;
			m_socket = sock;
		}
	}

	TCPSocket::~TCPSocket()
	{
		FB_LOGINFO("Closing TCP/IP socket ID %i.", g_socketId);
		closesocket(m_socket);
	}

	bool TCPSocket::isValid()
	{
		return m_socket != INVALID_SOCKET;
	}

	int TCPSocket::bind(const sockaddr* name, int namelen)
	{
		return ::bind(m_socket, name, namelen);
	}

	int TCPSocket::listen(int backlog)
	{
		return ::listen(m_socket, backlog);
	}

	TCPSocket* TCPSocket::accept(sockaddr* addr, int* addrlen)
	{
		return new TCPSocket(::accept(m_socket, addr, addrlen));
	}

	int TCPSocket::connect(sockaddr* name, int namelen)
	{
		return ::connect(m_socket, name, namelen);
	}

	int TCPSocket::recv(char* buf, int len, int flags)
	{
		return ::recv(m_socket, buf, len, flags);
	}

	int TCPSocket::send(const char* buf, int len, int flags)
	{
		return ::send(m_socket, buf, len, flags);
	}
}