#ifndef __TCPSERVER_H__
#define __TCPSERVER_H__

#include "TCPListener.h"

namespace Network
{
	class TCPServer
	{
	private:
		
		static TCPServer* m_pInstance;

	public:
		TCPListener* m_pListener;

		TCPServer(int port);

		bool start();

		static void processData(Network::SocketObj from, char* data, int length);
		virtual void handleData(Network::SocketObj from, char* data, int length) = 0;
	};
}

#endif /* __TCPSERVER_H__ */