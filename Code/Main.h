#ifndef __MAIN_H__
#define __MAIN_H__

#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS

// Typedefs
typedef __int64 int64;
typedef short int16;
typedef unsigned int uint;

// Libraries
#pragma comment(lib, "ws2_32.lib")

// Windows
#include <winsock2.h>
#include <windows.h>
#include <time.h>
#include <process.h>

// Standard
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <string>
#include <stack>
#include <vector>
#include <map>

#include "Log.h"

#endif /* __MAIN_H__ */