#include "Main.h"
#include "BlazeServerBase.h"

#include "TdfStream.h"

namespace Blaze
{
	ServerBase::ServerBase(int port)
		: TCPServer(port)
	{
	}

	void ServerBase::setName(std::string name)
	{
		m_name = name;
	}

	void ServerBase::addComponent(Blaze::Component* pComponent)
	{
		m_components.push_back(pComponent);
	}

	void ServerBase::handleData(Network::SocketObj from, char* data, int length)
	{
// 		FILE* fp = fopen("x:\\temp\\dmp\\blazehub.dmp", "wb");
// 		fwrite(data, length, 1, fp);
// 		fclose(fp);

		FB_LOGINFO("[%s] Received %i bytes from %s:%i.", 
			m_name.c_str(), length, inet_ntoa(from.m_sadr.sin_addr), htons(from.m_sadr.sin_port));
	
		Blaze::FireFrame* pFireframe = new Blaze::FireFrame();
		pFireframe->ReadData(data, length);

		TdfStream stream(pFireframe->mPayload, pFireframe->mLength);
		std::map<std::string, Tdf*> tdfMap;

		while (stream.GetPosition() < length)
		{
			Tdf* tdf = stream.ReadTdf();
		
			//FB_LOGINFO("%s", tdf->mLabel.c_str());
			//tdfMap.insert(std::make_pair(tdf->mLabel, tdf));
		}

		return;

		typedef std::vector<Blaze::Component*>::iterator mCompItr;

		bool bHandled = false;
		
		for (mCompItr itr = m_components.begin(); itr != m_components.end(); itr++)
		{
			Blaze::Component* pComponent = *itr;

			if (pComponent && pComponent->mComponentId == pFireframe->mComponentId)
			{
				pComponent->HandleData(from, pFireframe);
				bHandled = true;
			}
		}

		if (!bHandled)
		{
			FB_LOGWARNING("[%s] Unhandled component %i.",
				m_name.c_str(), pFireframe->mComponentId);
		}
	}
}