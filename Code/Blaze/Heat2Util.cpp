#include "Main.h"
#include "Heat2Util.h"

namespace Blaze
{
	void Heat2Util::makeTag(const char *tag)
	{
		if (tag)
		{

		}
	}

	void Heat2Util::decodeTag(uint32_t tag, char *buf, uint32_t len, const bool convertToLowercase)
	{
		if (buf && len >= 4)
		{
			buf[0] = (tag >> 26) ? (tag >> 26) + 32 : 0;
			buf[1] = (tag >> 20) & 0x3F ? ((tag >> 20) & 0x3F) + 32 : 0;
			buf[2] = (tag >> 14) & 0x3F ? ((tag >> 14) & 0x3F) + 32 : 0;
			buf[3] = BYTE1(tag) & 0x3F ? (BYTE1(tag) & 0x3F) + 32 : 0;

			buf[4] = 0;

			if (convertToLowercase)
			{
				for (int i = 0; i < len; ++i)
					buf[i] = _tolower(buf[i]);
			}
		}
	}
}