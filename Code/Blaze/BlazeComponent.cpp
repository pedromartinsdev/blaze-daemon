#include "Main.h"
#include "BlazeComponent.h"
#include "BlazeReply.h"

namespace Blaze
{
	Component::Component(int16 componentId)
		: mComponentId(componentId)
	{
	}

	void Component::AddCommand(Blaze::Command* pCommand)
	{
		m_commands.push_back(pCommand);
	}

	bool Component::HandleData(Network::SocketObj from, Blaze::FireFrame* pFireframe)
	{
		bool bHandled = false;

		typedef std::vector<Blaze::Command*>::iterator mCmdItr;

		for (mCmdItr itr = m_commands.begin(); itr != m_commands.end(); itr++)
		{
			Blaze::Command* pCommand = *itr;

			if (pCommand->mCommandId == pFireframe->mCommand)
			{
			//	Blaze::Reply& reply = pCommand->HandleData(pFireframe);

			//	reply.SendResponse(from);
				bHandled = true;
			}
		}

		if (!bHandled)
		{
			FB_LOGWARNING("Unhandled command %i from component %i.", pFireframe->mCommand, mComponentId);
		}

		return bHandled;
	}
}