#include "Main.h"
#include "BlazeReply.h"

namespace Blaze
{
	Reply::Reply(int16 componentId, int16 command, int16 errorCode, int16 msgType)
		: mComponentId(componentId), mCommand(command), mErrorCode(errorCode), mMsgType(msgType)
	{
		m_buffer = new fb::Buffer(NULL);
	}

	bool Reply::SendResponse(Network::SocketObj to)
	{
		FB_LOGINFO("Sending response to %s:%i.", inet_ntoa(to.m_sadr.sin_addr), htons(to.m_sadr.sin_port));

		return true;
	}
}