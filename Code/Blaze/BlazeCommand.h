#ifndef __BLAZECOMMAND_H__
#define __BLAZECOMMAND_H__

#include <map>
#include "BlazeReply.h"
#include "BlazeFireFrame.h"

namespace Blaze
{
	class Command
	{
	public:
		int16 mCommandId;

		Command(int16 commandId);

		virtual Blaze::Reply& HandleData(/*std::map<Blaze::Tdf*> request*/) = 0;
	};
}

#endif /* __BLAZECOMMAND_H__ */