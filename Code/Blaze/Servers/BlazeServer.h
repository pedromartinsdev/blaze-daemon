#ifndef __BLAZESERVER_H__
#define __BLAZESERVER_H__

#define BLAZE_SERVER_DEFAULT_PORT 42129

#include "BlazeServerBase.h"

namespace Blaze
{
	class Server : public Blaze::ServerBase
	{
	public:
		Server();
	};
}

#endif /* __BLAZESERVER_H__ */