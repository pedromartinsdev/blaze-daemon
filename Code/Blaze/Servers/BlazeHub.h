#ifndef __BLAZEHUB_H__
#define __BLAZEHUB_H__

#define BLAZE_HUB_DEFAULT_PORT 42127

#include "BlazeServerBase.h"

namespace Blaze
{
	class HubServer : public Blaze::ServerBase
	{
	public:
		HubServer();
	};
}

#endif /* __BLAZEHUB_H__ */