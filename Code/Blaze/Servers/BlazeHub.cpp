#include "Main.h"
#include "BlazeHub.h"

namespace Blaze
{
	HubServer::HubServer()
		: ServerBase(BLAZE_HUB_DEFAULT_PORT)
	{
		FB_LOGINFO("Initializing Blaze Hub.");

		setName("BlazeHub");

		// TODO: Add components here
	}
}