#ifndef __BLAZEREPLY_H__
#define __BLAZEREPLY_H__

#include "TCPSocketStack.h"
#include "Buffer.h"

namespace Blaze
{
	class Reply
	{
	private:
		fb::Buffer* m_buffer;

		int16 mComponentId;
		int16 mCommand;
		int16 mErrorCode;
		int16 mMsgType;
		int16 mMsgId;
		char* mPayload;
		int mLength;

	public:
		Reply(int16 componentId, int16 command, int16 errorCode, int16 msgType);

		bool SendResponse(Network::SocketObj to);
	};
}

#endif /* __BLAZEREPLY_H__ */