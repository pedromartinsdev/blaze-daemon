#ifndef __BLAZECOMPONENT_H__
#define __BLAZECOMPONENT_H__

#include <vector>
#include "TCPSocketStack.h"
#include "BlazeFireFrame.h"
#include "BlazeCommand.h"

namespace Blaze
{
	class Component
	{
	protected:
		std::vector<Blaze::Command*> m_commands;
		void AddCommand(Blaze::Command* pCommand);

	public:
		int16 mComponentId;

		Component(int16 componentId);
		bool HandleData(Network::SocketObj from, Blaze::FireFrame* pFireframe);
	};
}

#endif /* __BLAZECOMPONENT_H__ */