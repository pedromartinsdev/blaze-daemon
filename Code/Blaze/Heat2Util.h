#ifndef __HEAT2UTIL_H__
#define __HEAT2UTIL_H__

#define BYTEn(x, n) (*((BYTE*)&(x)+n))
#define BYTE1(x) BYTEn(x, 1)

namespace Blaze
{
	class Heat2Util
	{
	public:
		static void makeTag(const char *tag);
		static void decodeTag(uint32_t tag, char *buf, uint32_t len, const bool convertToLowercase);
	};
}

#endif /* __HEAT2UTIL_H__ */