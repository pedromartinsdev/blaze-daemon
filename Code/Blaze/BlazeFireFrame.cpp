#include "Main.h"
#include "BlazeFireFrame.h"

namespace Blaze
{
	bool FireFrame::ReadData(char* data, int length)
	{
		if (length <= 0xF)
		{
			FB_LOGERROR("Packet is too small to be read.");
			return false;
		}

		mSize			= HIBYTE(*(short*)data);
		mComponentId	= HIBYTE(*(short*)&data[2]);
		mCommand		= HIBYTE(*(short*)&data[4]);
		mErrorCode		= HIBYTE(*(short*)&data[6]);
		mMsgType		= HIBYTE(*(short*)&data[8]);
		mMsgId			= HIBYTE(*(short*)&data[10]);

		// *ignore nulled extra length*

		mLength = length - 12;
		mPayload = (char*)malloc(mSize);

		memcpy(mPayload, &data[12], mSize);

		return true;
	}
}