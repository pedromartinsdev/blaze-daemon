#ifndef __BLAZEFIREFRAME_H__
#define __BLAZEFIREFRAME_H__

namespace Blaze
{
	class FireFrame
	{
	public:
		int16 mSize;
		int16 mComponentId;
		int16 mCommand;
		int16 mErrorCode;
		int16 mMsgType;
		int16 mMsgId;
		char* mPayload;
		int mLength;

		bool ReadData(char* data, int length);
	};
}

#endif /* __BLAZEFIREFRAME_H__ */