#ifndef __BLAZESERVERBASE_H__
#define __BLAZESERVERBASE_H__

#include "TCPServer.h"
#include "BlazeComponent.h"

namespace Blaze
{
	class ServerBase : public Network::TCPServer
	{
	protected:
		std::vector<Blaze::Component*> m_components;

	public:
		std::string m_name;

		ServerBase(int port);

		void setName(std::string name);
		void addComponent(Blaze::Component* pComponent);
		void handleData(Network::SocketObj from, char* data, int length);
	};
}

#endif /* __BLAZESERVERBASE_H__ */