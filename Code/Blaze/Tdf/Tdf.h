#ifndef __TDF_H__
#define __TDF_H__

#include <string>

namespace Blaze
{
	enum TdfBaseType : BYTE
	{
		TDF_TYPE_MIN = 0x0,
		TDF_TYPE_INTEGER = 0x0,
		TDF_TYPE_STRING = 0x1,
		TDF_TYPE_BINARY = 0x2,
		TDF_TYPE_STRUCT = 0x3,
		TDF_TYPE_LIST = 0x4,
		TDF_TYPE_MAP = 0x5,
		TDF_TYPE_UNION = 0x6,
		TDF_TYPE_VARIABLE = 0x7,
		TDF_TYPE_BLAZE_OBJECT_TYPE = 0x8,
		TDF_TYPE_BLAZE_OBJECT_ID = 0x9,
		TDF_TYPE_FLOAT = 0xA,
		TDF_TYPE_TIMEVALUE = 0xB,
		TDF_TYPE_MAX = 0xC,
	};

	class Tdf
	{
	public:
		TdfBaseType mType;
		std::string mLabel;
	};

	class TdfString : public Tdf
	{
	public:
		TdfString(std::string& label, std::string str)
		{
			mLabel = label;
			mString = str;
			mType = TdfBaseType::TDF_TYPE_STRING;
		}

		std::string mString;
	};

	class TdfInteger : public Tdf
	{
	public:
		TdfInteger(std::string& label, int value)
		{
			mLabel = label;
			mValue = value;
			mType = TdfBaseType::TDF_TYPE_INTEGER;
		}

		int mValue;
	};

	class TdfStruct : public Tdf
	{
	public:
		TdfStruct(std::string& label, std::map<std::string, Tdf*>& map)
		{
			mLabel = label;
			mMap = map;
			mType = TdfBaseType::TDF_TYPE_STRUCT;
		}

		std::map<std::string, Tdf*> mMap;
	};

	class TdfMin : public Tdf
	{
	public:
		TdfMin(std::string& label, BYTE value)
		{
			mLabel = label;
			mValue = value;
			mType = TdfBaseType::TDF_TYPE_MIN;
		}

		BYTE mValue;
	};
}

#endif /* __TDF_H__ */