#ifndef __TDFSTREAM_H__
#define __TDFSTREAM_H__

#include <string>

#include "Buffer.h"
#include "InStream.h"

#include "Tdf.h"

namespace Blaze
{
	class TdfStream
	{
	private:
		fb::Buffer		*mBuffer;
		fb::InStream	mStream;

	public:
		TdfStream(char* data, int size);

		int GetPosition();

		
		Tdf* ReadTdf();

		Tdf* ReadUnknown(TdfBaseType type, std::string label);
		
		TdfMin* ReadMin(std::string label);
		TdfString* ReadString(std::string label);
		TdfInteger* ReadInteger(std::string label);
		TdfStruct* ReadStruct(std::string label);


		std::string ReadTag(bool useFlags, int* onlyFlags);
	};
}

#endif /* __TDFSTREAM_H__ */