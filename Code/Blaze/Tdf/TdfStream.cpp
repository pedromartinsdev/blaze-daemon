#include "Main.h"

#include "Buffer.h"
#include "InStream.h"

#include "Tdf.h"
#include "TdfStream.h"

#include "boost/lexical_cast.hpp"

#define BETWEEN(x, x_min, x_max) ((x) > (x_min) && (x) < (x_max))

namespace Blaze
{

	std::string toString(int n)
	{
		using boost::lexical_cast;
		return lexical_cast<std::string>(n);
	}

	TdfStream::TdfStream(char* data, int size)
	{
		mBuffer = new fb::Buffer(data, size);
		mStream.initialize(mBuffer);
	}

	int TdfStream::GetPosition()
	{
		return mStream.getBuffer()->getPosition();
	}

	std::string TdfStream::ReadTag(bool useFlags, int* onlyFlags)
	{
		int tag = 0;
		std::string stag = "";

		// Read the tag (which is 3 bytes long)
		mStream.read(&tag, 3);

		// MIN and INTEGER tdf's have the same type (which is 0)
		// so they hide flags in the tags so we can distinguish
		// whether we're dealing with MIN's or INTEGER's.
		// 0 = integer
		// 1 = min

		if (tag & 1)
		{
			printf("Yes:)\n");
			//tag &= 0xFFFFFFFE;
		}
		else
		{
			printf("No:(\n");
		}

		tag = _byteswap_ulong(tag) >> 8;

		stag += (((tag >> 18) & 0x3F) & 0x1F) | 64;
		stag += (((tag >> 12) & 0x3F) & 0x1F) | 64;
		stag += (((tag >> 6) & 0x3F) & 0x1F) | 64;
		stag += ((tag & 0x3F) & 0x1F) | 64;

		/*stag += ((tag >> 26) + 32);
		stag += ((tag >> 20) & 0x3F) + 32;
		stag += ((tag >> ))*/

		return stag;
	}

	Tdf* TdfStream::ReadTdf()
	{
		TdfBaseType type;

		int onlyFlags = 0;

		std::string label = ReadTag(
			!mBuffer->m_buffer[mBuffer->getPosition() + 3] ? true : false, &onlyFlags);

		mStream.read(&type, 1);

		FB_LOGINFO("[%i] %d: %s\n", mBuffer->getPosition(), type, label.c_str());

		switch (type)
		{
		case TDF_TYPE_INTEGER:
			if (onlyFlags & 1)
				return ReadMin(label);
			else
				return ReadInteger(label);

		case TDF_TYPE_STRING:
			return ReadString(label);

		case TDF_TYPE_STRUCT:
			return ReadStruct(label);

		default:
			return ReadUnknown(type, label);
		}

		return nullptr;
	}

	Tdf* TdfStream::ReadUnknown(TdfBaseType type, std::string label)
	{
		Tdf* pUnkTdf = new Tdf();
		pUnkTdf->mType = type;
		pUnkTdf->mLabel = label;

		int pos = mBuffer->getPosition();

		// Read 'till the end of Tdf
		while (mBuffer->m_buffer[mBuffer->getPosition()] != 0)
		{
			mBuffer->skip(1);
		}

		mBuffer->skip(1);

		FB_LOGWARNING("Ignored unsupported TDF of type %d (was %i bytes long).",
			type, (mBuffer->getPosition() - pos));

		return pUnkTdf;
	}

	TdfString* TdfStream::ReadString(std::string label)
	{
		char len = 0xFF;
		std::string str = "";

		mStream >> len;

		len--;

		for (int i = 0; i < len; i++)
		{
			char b = 0xFF;
			mStream >> b;

			str += b;
		}

		mStream.skip(1);

		return new TdfString(label, str);
	}

	TdfMin* TdfStream::ReadMin(std::string label)
	{
		BYTE value = 0;

		printf("%i\n", mBuffer->getPosition());

		// This one is very simple
		mStream >> value;

		printf("%d\n", value);

		return new TdfMin(label, value);
	}

	TdfInteger* TdfStream::ReadInteger(std::string label)
	{
		BYTE neg = (mBuffer->m_buffer[mBuffer->getPosition()] >> 6) & 1;

		int value = 0,
			shift = 6;

		for (int i = 0; i < 8; i++)
		{
			BYTE byte = 0;
			mStream >> byte;

			if (byte > 128)
				break;

			value |= (int)(byte & 0x7F) << shift;
			shift += 7;
		}
		
		mStream.skip(1);

		return new TdfInteger(label, value);
	}

	TdfStruct* TdfStream::ReadStruct(std::string label)
	{
		std::map<std::string, Tdf*> map;
		fb::Buffer* pBuffer = mStream.getBuffer();

		while (pBuffer->m_buffer[pBuffer->getPosition()] != 0)
		{
			Tdf* tdf = ReadTdf();

			map.insert(std::make_pair(tdf->mLabel, tdf));
		}

		mStream.skip(1);

		return new TdfStruct(label, map);
	}
}