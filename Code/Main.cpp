#include "Main.h"
#include "Log.h"
#include "WinMainHelper.h"

#include "BlazeHub.h"
#include "BlazeServer.h"

#include "BlazeComponent.h"

void SetWindow(int Width, int Height)
{
	_COORD coord;
	coord.X = Width;
	coord.Y = Height;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = Height - 1;
	Rect.Right = Width - 1;

	// Get handle of the standard output 
	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);

	// Set screen buffer size to that specified in coord 
	SetConsoleScreenBufferSize(Handle, coord);

	// Set the window size to that specified in Rect 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);
}

// ==========

#define SHIDWORD(x)  (*((long*)&(x)+1))

namespace Blaze
{
	// =======================
	const class TdfTagInfo
	{		

	public:
		unsigned int mTag;
		const char* mMember;
		unsigned int mMemberIndex;

		unsigned int getTag();
		const char* getMember();
		unsigned int getMemberIndex();
	};

	unsigned int TdfTagInfo::getTag()
	{
		return mTag;
	}

	const char* TdfTagInfo::getMember()
	{
		return mMember;
	}

	unsigned int TdfTagInfo::getMemberIndex()
	{
		return mMemberIndex;
	}
	// =======================

	const class TdfTagInfoMap
	{
	private:
		Blaze::TdfTagInfo* mTagInfo;

	public:
		unsigned int mSize;

		Blaze::TdfTagInfo*	find(const char* member);

		Blaze::TdfTagInfo*	get(unsigned int tag);
		const char*			getUnionMember(unsigned int activeIndex);
	};

	Blaze::TdfTagInfo* TdfTagInfoMap::find(const char* member)
	{
		if (mSize)
		{
			Blaze::TdfTagInfo* pTagInfo = mTagInfo;
			const char** pMember = &mTagInfo->mMember;

			int i = 0;

			while (_stricmp(*pMember, member))
			{
				i++;
				pMember += 3;

				if (i >= mSize)
					return nullptr;
			}

			return &mTagInfo[i];
		}

		return nullptr;
	}

	Blaze::TdfTagInfo* TdfTagInfoMap::get(unsigned int tag)
	{
		if (mSize)
		{
			Blaze::TdfTagInfo* pTagInfo = mTagInfo;

			int i = 0;

			while (pTagInfo->getTag() != tag)
			{
				i++;
				pTagInfo++;

				if (i >= mSize)
					return nullptr;
			}

			return &mTagInfo[i];
		}

		return nullptr;
	}

	const char* TdfTagInfoMap::getUnionMember(unsigned int activeIndex)
	{
		if (mSize)
		{
			Blaze::TdfTagInfo* pTagInfo = mTagInfo;
			int index = mTagInfo->getMemberIndex();

			int i = 0;

			while (mTagInfo->getMemberIndex() != activeIndex)
			{
				i++;
				index += 12;

				if (i >= mSize)
					return 0;
			}

			return mTagInfo[i].getMember();
		}

		return 0;
	}

	// =======================

	struct TdfMemberInfo
	{
		unsigned int tagAndFlags;
		char type;
		char offsetNext;
		unsigned short offset;
	};

	class RawBuffer
	{
	public:
		char* mHead;
		char* mData;
		char* mTail;
		char* mEnd;
		char* mMark;
		bool mOwnMem;
	};

	class Tdf
	{
	public:
		void print(char* buf, unsigned bufsize, int indent);

		// Virtual functions
		virtual Blaze::Tdf*				clone(char memGroupId) = 0;
		virtual Blaze::TdfTagInfoMap*	getTagInfoMap() = 0;
		virtual const char*				getClassName() = 0;
		virtual const char*				getFullClassName() = 0;
		virtual unsigned int			getTdfId() = 0;
		virtual bool					isRegisteredTdf() = 0;
		virtual Blaze::TdfMemberInfo*	getMemberInfo();
	};

	void Tdf::print(char* buf, unsigned bufsize, int indent)
	{
		Blaze::RawBuffer raw;

		// Initialize RawBuffer
		raw.mOwnMem = 0;
		raw.mHead = buf;
		raw.mData = buf;
		raw.mTail = buf;
		raw.mEnd = buf ? &buf[bufsize] : 0;
		raw.mMark = buf;

		// TODO: implement Blaze::PrintEncoder and finish this.
	}

	// =======================

	class TdfVisitor;

	class VariableTdfBase
	{
	private:
		Blaze::Tdf* mTdf;
		char mTdfMemGroup;
		char mMemGroup;
		bool mTdfIsOwned;

	public:
		VariableTdfBase(char memGroupId, bool isOwned);
		~VariableTdfBase();

		bool isValid();
		
		Blaze::Tdf* get();

		void set(Blaze::Tdf* tdf, bool takesOwnership, char memGroupId);
		void copyInto(Blaze::TdfVisitor* visitor, Blaze::Tdf* rootTdf, Blaze::Tdf* parentTdf, unsigned tag, Blaze::VariableTdfBase* copy);

		void clear();

		// Virtual function
		virtual void create(unsigned int tdfid, bool isOwned) = 0;
	};

	VariableTdfBase::VariableTdfBase(char memGroupId, bool isOwned)
	{
		mTdf = 0;
		mTdfMemGroup = 0;
		mMemGroup = memGroupId;
		mTdfIsOwned = isOwned;
	}

	VariableTdfBase::~VariableTdfBase()
	{
		clear();
	}

	bool VariableTdfBase::isValid()
	{
		return mTdf != 0;
	}

	Blaze::Tdf* VariableTdfBase::get()
	{
		return mTdf;
	}

// 	void VariableTdfBase::create(unsigned int tdfid, bool isOwned)
// 	{
// 		if (tdfid)
// 		{
// 			clear();
// 
// 
// 		}
// 	}

	void VariableTdfBase::set(Blaze::Tdf* tdf, bool takesOwnership, char memGroupId)
	{
		if (mTdf)
		{
			if (mTdfIsOwned)
			{
				free(mTdf);
			}
		}

		mTdf = tdf;
		mTdfIsOwned = takesOwnership;
		mTdfMemGroup = memGroupId;
	}

	void VariableTdfBase::copyInto(Blaze::TdfVisitor* visitor, Blaze::Tdf* rootTdf, Blaze::Tdf* parentTdf, unsigned tag, Blaze::VariableTdfBase* copy)
	{
		if (this != copy)
		{
			copy->clear();

			if (mTdf)
			{
				Blaze::Tdf* tdf = mTdf->clone(mMemGroup);
				copy->set(tdf, true, mMemGroup);

				visitor->visit(rootTdf, parentTdf, tag, copy->mTdf, mTdf);
			}
		}
	}

	void VariableTdfBase::clear()
	{
		if (mTdf && mTdfIsOwned)
		{
			free(mTdf);
		}
	}

	// =======================

	class TdfUnion : Blaze::Tdf
	{
	private:
		char mMemGroup;

	public:
		TdfUnion(char memGroupId);

		void switchActiveMember(unsigned member);
	};

	TdfUnion::TdfUnion(char memGroupId)
		: mMemGroup(memGroupId)
	{
	}

	void TdfUnion::switchActiveMember(unsigned member)
	{
	}

	// =======================

	struct TdfEnumInfo
	{
		const char* mName;
		int mValue;
	};

	class TdfEnumMap
	{
	private:
		Blaze::TdfEnumInfo* mEntries;
		unsigned int mCount;

	public:
		TdfEnumMap(Blaze::TdfEnumInfo* entries, unsigned count);

		bool exists(int value);
		bool findByName(const char* name, int* value);
		bool findByValue(int value, const char **name);
	};


	TdfEnumMap::TdfEnumMap(Blaze::TdfEnumInfo* entries, unsigned count)
	{
		mEntries = entries;
		mCount = count;
	}

	bool TdfEnumMap::exists(int value)
	{
		if (mCount)
		{
			int i = 0,
				val = mEntries->mValue;

			while (val != value)
			{
				i++;
				val += 8;

				if (i >= mCount)
					return false;
			}

			return true;
		}

		return false;
	}

	bool TdfEnumMap::findByName(const char* name, int* value)
	{
		if (name)
		{
			if (mCount)
			{
				int i = 0;
				const char** pName = &mEntries->mName;

				while (_stricmp(*pName, name))
				{
					i++;
					pName += 2;

					if (i >= mCount)
						return false;
				}

				*value = mEntries[i].mValue;
			}
		}

		return false;
	}

	bool TdfEnumMap::findByValue(int value, const char **name)
	{
		if (mCount)
		{
			int i = 0, 
				val = mEntries->mValue;

			while (val != value)
			{
				i++;
				val += 8;

				if (i > mCount)
					goto failure;
			}

			if (name)
				*name = mEntries[i].mName;

			return true;
		}

	failure:
		if (name)
			*name = "UNKNOWN";
		
		return false;
	}

	// =======================

	class TdfBlob
	{
	private:
		unsigned int mSize;
		char* mData;
		unsigned int mCount;
		char mOwned : 1;
		char mMemGroup;

	public:
		TdfBlob(char memGroupId);
		TdfBlob(Blaze::TdfBlob* rhs, char memGroupId);
		~TdfBlob();

		void assignData(char* data, unsigned len);
		bool resize(unsigned newSize, bool toPreserve);

		bool setData(const char* value, unsigned len);
		void copyInto(Blaze::TdfBlob* newBlob);
		Blaze::TdfBlob* clone(char memGroupId);

		char* getData();
		unsigned int getSize();

		unsigned int getCount();
		void setCount(unsigned count);
	};

	TdfBlob::TdfBlob(char memGroupId)
	{
		mOwned |= 1;

		mSize = 0;
		mData = 0;
		mCount = 0;
		mMemGroup = memGroupId;
	}

	TdfBlob::TdfBlob(Blaze::TdfBlob* rhs, char memGroupId)
	{
		mOwned |= 1;

		mSize = rhs->mSize;
		mData = 0;
		mCount = rhs->mCount;
		mMemGroup = memGroupId;

		if (rhs->mData)
		{
			mData = (char *)malloc(mSize);
			memcpy(mData, rhs->mData, mSize);
		}
	}

	TdfBlob::~TdfBlob()
	{
		if (mData)
		{
			if (mOwned & 1)
			{
				free(mData);
			}
		}
	}

	void TdfBlob::assignData(char* data, unsigned len)
	{
		if (mData && mOwned & 1)
		{
			free(mData);
		}

		mData = data;
		mSize = len;
		mCount = len;
	}

	bool TdfBlob::resize(unsigned newSize, bool toPreserve)
	{
		if (newSize)
		{
			if (newSize == mSize)
				return true;

			char* data = (char *)malloc(newSize);

			if (mData)
			{
				if (toPreserve && newSize > mCount)
					memcpy(data, mData, mCount);

				if (mOwned & 1)
				{
					free(mData);
				}
			}

			mOwned |= 1;
			mData = (char *)data;
			mSize = newSize;

			return true;
		}

		return false;
	}

	bool TdfBlob::setData(const char* value, unsigned len)
	{
		if (value)
		{
			if (resize(len, false))
			{
				memcpy(mData, value, len);
				mOwned |= 1;

				return true;
			}
		}
		else
		{
			if (mData)
			{
				if (mOwned & 1)
				{
					free(mData);
				}

				mData = 0;
			}

			mSize = 0;
			mCount = 0;

			return true;
		}

		return false;
	}

	void TdfBlob::copyInto(Blaze::TdfBlob* newBlob)
	{
		if (this != newBlob)
		{
			if (mCount)
			{
				newBlob->resize(mSize, false);
				memcpy(newBlob->mData, mData, mCount);
				newBlob->mCount = mCount;
			}
			else
			{
				if (mData)
				{
					if (mOwned & 1)
					{
						free(mData);
					}
					mData = 0;
				}

				mSize = 0;
				mCount = 0;
			}
		}
	}

	Blaze::TdfBlob* TdfBlob::clone(char memGroupId)
	{
		Blaze::TdfBlob* pTdfBlob = new Blaze::TdfBlob(memGroupId);

		copyInto(pTdfBlob);
		return pTdfBlob;
	}

	char* TdfBlob::getData()
	{
		return mData;
	}

	unsigned int TdfBlob::getSize()
	{
		return mSize;
	}

	unsigned int TdfBlob::getCount()
	{
		return mCount;
	}

	void TdfBlob::setCount(unsigned count)
	{
		mCount = count;
	}

	// =======================

	char EMPTY_STR[16] = { 0 };

	class TdfString
	{
	private:
		char *mTdfStringPointer;
		unsigned int mLength;
		char mMemGroup;
		char mOwned : 1;
		char mDeepCopy : 1;

	public:
		TdfString(char memGroupId);
		TdfString(Blaze::TdfString* rhs, char memGroupId);
		TdfString(const char* value, char memGroupId);
		~TdfString();

		int length();
		const char* c_str();

		void release();
		void set(const char* value, unsigned len);
		char* assignBuffer(char* buffer);
		char* copyToBuffer(const char* inputString, char* buffer);

		bool operator<(Blaze::TdfString* rhs);
		Blaze::TdfString* operator=(Blaze::TdfString* rhs);
		Blaze::TdfString* operator=(const char* rhs);
	};

	TdfString::TdfString(char memGroupId)
	{
		mTdfStringPointer = EMPTY_STR;
		mLength = 0;
		mMemGroup = memGroupId;
		mOwned &= 0xFE | 2;
	}

	TdfString::TdfString(Blaze::TdfString* rhs, char memGroupId)
	{
		mTdfStringPointer = EMPTY_STR;
		mMemGroup = memGroupId;
		mOwned &= 0xFE | 2;

		if (rhs->mLength)
		{
			if (rhs->mOwned & 2)
			{
				mTdfStringPointer = (char *)malloc(rhs->mLength + 1);

				mOwned |= 1;
				mLength = rhs->mLength;

				return;
			}

			mTdfStringPointer = rhs->mTdfStringPointer;
			mOwned &= 0xFD;
		}

		mLength = rhs->mLength;
	}

	TdfString::TdfString(const char* value, char memGroupId)
	{
		mOwned &= 0xFE | 2;
		mTdfStringPointer = EMPTY_STR;
		mLength = 0;
		mMemGroup = 0;

		set(value, 0);
	}

	TdfString::~TdfString()
	{
		release();
	}

	int TdfString::length()
	{
		return mLength;
	}

	const char* TdfString::c_str()
	{
		return mTdfStringPointer;
	}

	void TdfString::release()
	{
		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		mLength = 0;
		mTdfStringPointer = EMPTY_STR;
	}

	void TdfString::set(const char* value, unsigned len)
	{
		if (value != mTdfStringPointer)
		{
			if (value && *value)
			{
				if (!len)
					len = strlen(value);

				if (!(mOwned & 1))
				{
					mTdfStringPointer = (char *)malloc(len + 1);
					mOwned |= 1;
				}

				if (len <= mLength)
				{
					memcpy(mTdfStringPointer, value, len);
					mTdfStringPointer[len] = 0;
					mLength = len;

					return;
				}
			}

			if (mOwned & 1)
				*mTdfStringPointer = *EMPTY_STR;
			else
				mTdfStringPointer = EMPTY_STR;

			mLength = 0;
		}
	}

	char* TdfString::assignBuffer(char* buffer)
	{
		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		if (buffer)
		{
			mTdfStringPointer = buffer;
			mLength = strlen(buffer);

			return &buffer[mLength + 1];
		}

		mTdfStringPointer = EMPTY_STR;
		mLength = 0;

		return 0;
	}

	char* TdfString::copyToBuffer(const char* inputString, char* buffer)
	{
		if (!buffer)
			return 0;

		if (mOwned & 1)
		{
			free(mTdfStringPointer);
			mOwned &= 0xFE;
		}

		mLength = strlen(inputString);

		if (mLength != -1)
		{
			if (!inputString)
			{
				*buffer = 0;
				mTdfStringPointer = buffer;

				return &buffer[mLength + 1];
			}

			strncpy(buffer, inputString, mLength + 1);
			buffer[mLength] = 0;
		}

		mTdfStringPointer = buffer;
		return &buffer[mLength + 1];
	}

	bool TdfString::operator<(Blaze::TdfString* rhs)
	{
		return strcmp(mTdfStringPointer, rhs->mTdfStringPointer) < 0;
	}

	Blaze::TdfString* TdfString::operator=(Blaze::TdfString* rhs)
	{
		set(rhs->mTdfStringPointer, rhs->mLength);
		return this;
	}

	Blaze::TdfString* Blaze::TdfString::operator=(const char* rhs)
	{
		set(rhs, 0);
		return this;
	}

	// =======================

	class TdfVisitor
	{
	public:
		virtual bool visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::VariableTdfBase *, Blaze::VariableTdfBase *) = 0;
 		virtual bool visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfUnion *, Blaze::TdfUnion *) = 0;
		virtual bool visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::Tdf *, Blaze::Tdf *) = 0;
 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, int *, const int, Blaze::TdfEnumMap *, const int) = 0;
 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfBlob *, Blaze::TdfBlob *) = 0;
 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfString *, Blaze::TdfString *, const char *, const unsigned int) = 0;
// 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfBitfield *, Blaze::TdfBitfield *) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, float *, const float, const float) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, unsigned __int64 *, const unsigned __int64, const unsigned __int64) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, __int64 *, const __int64, const __int64) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, unsigned int *, const unsigned int, const unsigned int) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, int *, const int, const int) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, unsigned __int16 *, const unsigned __int16, const unsigned __int16) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, __int16 *, const __int16, const __int16) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, char *, const char, const char) = 0;
		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, bool *, const bool, const bool) = 0;
// 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfMapBase *, Blaze::TdfMapBase *) = 0;
// 		virtual void visit(Blaze::Tdf *, Blaze::Tdf *, unsigned int, Blaze::TdfVectorBase *, Blaze::TdfVectorBase *) = 0;
 		virtual bool visit(Blaze::TdfUnion *, Blaze::TdfUnion *) = 0;
 		virtual bool visit(Blaze::Tdf *, Blaze::Tdf *) = 0;
	};

	// =======================

	class Encoder
	{
	public:
		static enum Type
		{
			READ,
			OVERWRITE,
			APPEND,
			INVALID
		};

		// Virtual functions
		virtual const char* getName() = 0;
		virtual Blaze::Encoder::Type getType() = 0;
	};

	// =======================

	class TdfEncoder : Encoder, TdfVisitor
	{
	private:
		Blaze::RawBuffer* mBuffer;

	protected:
		unsigned int mErrorCount;
	
	public:
		TdfEncoder();
		
		void setBuffer(Blaze::RawBuffer* buffer);
		bool encode(Blaze::RawBuffer* buffer, Blaze::Tdf* tdf);
	};

	TdfEncoder::TdfEncoder()
	{
		mErrorCount = 0;
		mBuffer = 0;
	}

	void TdfEncoder::setBuffer(Blaze::RawBuffer* buffer)
	{
		mBuffer = buffer;
	}

	bool TdfEncoder::encode(Blaze::RawBuffer* buffer, Blaze::Tdf* tdf)
	{
		bool ok = visit(tdf, tdf);
		mBuffer = 0;
		return ok;
	}

	// =======================

	class Heat2Encoder : Blaze::TdfEncoder
	{
	private:
		Blaze::RawBuffer* mBuffer;
		bool mEncodeHeader;
		unsigned int mEncodedStringSize;

	public:
		Heat2Encoder();

		const char* getClassName();
		Blaze::Encoder::Type getType();

		void setBuffer(Blaze::RawBuffer* buffer);

		int encodeSize();

		bool visit(Blaze::Tdf* tdf, Blaze::Tdf* referenceValue);
	};

	Heat2Encoder::Heat2Encoder()
	{
		mErrorCount = 0;
		mBuffer = 0;
		mEncodeHeader = 0;
		mEncodedStringSize = 0;
	}

	const char* Heat2Encoder::getClassName()
	{
		return "heat2";
	}

	Blaze::Encoder::Type Heat2Encoder::getType()
	{
		return Blaze::Encoder::Type::INVALID;
	}

	void Heat2Encoder::setBuffer(Blaze::RawBuffer* buffer)
	{
		mBuffer = buffer;
		mErrorCount = 0;
		mEncodeHeader = true;
		mEncodedStringSize = 0;
	}

	int Heat2Encoder::encodeSize()
	{
		return mBuffer ? mBuffer->mTail - mBuffer->mData : 0;
	}

	bool Heat2Encoder::visit(Blaze::Tdf* tdf, Blaze::Tdf* referenceValue)
	{
		printf("NOT IMPLEMENTED\n");
		/*return visit(tdf, referenceValue);*/
		return true;
	}

	// =======================
}
// 
// class Encoder
// {
// 
// };
// 
// class TdfEncoder : Encoder, Blaze::TdfVisitor
// {
// public:
// 	unsigned int	mErrorCount;
// 	RawBuffer		*mBuffer;
// 
// 	TdfEncoder();
// 
// 	void encode(RawBuffer* buffer, Tdf* tdf);
// };
// 
// TdfEncoder::TdfEncoder()
// {
// 	mErrorCount = 0;
// 	mBuffer = 0;
// }
// 
// void TdfEncoder::encode(RawBuffer* buffer, Tdf* tdf)
// {
// 
// }
// 
// class Heat2Encoder : TdfEncoder
// {
// private:
// 	RawBuffer		*mBuffer;
// 	bool			mEncodeHeader;
// 	unsigned int	mEncodedStringSize;
// 
// public:
// 	void encodeVarsizeInteger(long long value);
// };
// 
// void Heat2Encoder::encodeVarsizeInteger(long long value)
// {
// 	if (mBuffer)
// 	{
// 		char* tail = mBuffer->mTail;
// 
// 		if (value)
// 		{
// 			int neg = 0;
// 			BYTE byte = 0;
// 
// 			if (value >= 0)
// 			{
// 				byte = (value & 0x3F) | 0x80;
// 			}
// 			else
// 			{
// 				neg = -value;
// 				byte = value | 0xC0;
// 			}
// 
// 			*tail = byte;
// 
// 			int i = 1;
// 			long long shift = neg >> 6;
// 
// 			if (SHIDWORD(shift) >= 0 && (SHIDWORD(shift) > 0 || shift))
// 			{
// 				do 
// 				{
// 					do 
// 					{
// 						tail[i++] = shift | 0x80;
// 						shift >>= 7;
// 					} while (SHIDWORD(shift) > 0);
// 				} while (SHIDWORD(shift) >= 0 && shift);
// 			}
// 
// 			tail[i - 1] &= 0x7F;
// 			mBuffer->mTail += i;
// 		}
// 		else
// 		{
// 			*tail = 0;
// 			mBuffer->mTail++;
// 		}
// 	}
// 	else
// 	{
// 		mErrorCount++;
// 	}
// }

// ==========

int main(int argc, char * argv[])
{


#if defined(_WIN32) && !_WIN32
	SetWindow(110, 40);

	// Initialize Frostbite console
	_beginthread(fb::_loop, 0, NULL);
	Sleep(100);

	FB_LOGINFO("Hello world!\n");

	FB_LOGINFO("Initializing WinSock...");
	WSAStartup(0x0202, new WSADATA);

	Blaze::Server		*pServer	= new Blaze::Server();
	Blaze::HubServer	*pHubServer = new Blaze::HubServer();

	Sleep(200);

	if (!pHubServer->start() || !pServer->start())
	{
		FB_LOGERROR("Blaze servers failed to start.");
	}

	getchar();

	delete pServer;
	delete pHubServer;
	
#endif

	return 0;
}