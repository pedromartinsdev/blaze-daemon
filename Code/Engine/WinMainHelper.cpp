#include "Main.h"
#include "buildnumber.h"
#include "WinMainHelper.h"

namespace fb
{
	HWND g_listBox = NULL;
	HWND g_commandBox = NULL;
	HWND g_toggleButtonBox = NULL;

	int g_listBoxItemIndex = 0;

	static LRESULT CALLBACK windowProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case 0x8003:
			break;

		case WM_COMMAND:

			break;

		case WM_CREATE:
			SendMessageA(g_listBox, LB_ADDSTRING, 0, (LPARAM)"Yoo");
			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}

		return NULL;
	}

	static HWND _createListBox(HINSTANCE hInstance, HWND hWnd, int width, int height)
	{
		return CreateWindowExA(
			0,
			"LISTBOX",
			0,
			0x50201000,
			0,
			0,
			width,
			height,
			hWnd,
			0,
			hInstance,
			0);
	}

	static HWND _createCommandBox(HINSTANCE hInstance, HWND hWnd, int x, int y, int width, int height)
	{
		return CreateWindowExA(
			0x200,
			"EDIT",
			0,
			0x50000000,
			x,
			y,
			width,
			height,
			hWnd,
			0,
			hInstance,
			0);
	}

	static HWND _createToggleButtonBox(HINSTANCE hInstance, HWND hWnd, int x, int y, int width, int height)
	{
		return CreateWindowExA(
			0,
			"BUTTON",
			0,
			0x54000000,
			x,
			y,
			width,
			height,
			hWnd,
			0,
			hInstance,
			0);
	}

	void _logToConsole(fb::StringBuilder builder)
	{
		SendMessageA(g_listBox, LB_INSERTSTRING, g_listBoxItemIndex++, (LPARAM)builder.m_string.c_str());
	}

	bool _createWindow(HINSTANCE hInstance)
	{
		WNDCLASSW wc;

		memset(&wc, 0, sizeof(wc));

		wc.style = 0;
		wc.cbClsExtra = 0;
		wc.lpfnWndProc = windowProcedure;
		wc.cbWndExtra = 8;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIconA(NULL, (LPCSTR)1);
		wc.hCursor = LoadCursorA(NULL, (LPCSTR)IDC_ARROW);
		wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
		wc.lpszClassName = L"frostbite";

		// TODO: assert(RegisterClass(&wc));
		RegisterClass(&wc);

		int width = 800, height = 600;

		RECT wr = { 0, 0, width, height };
		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

		char title[256];
		sprintf(title, "Blaze Daemon PM [Build %04i]", BUILDNUMBER);

		HWND hWnd = CreateWindowExA(
			0,
			"frostbite",
			title,
			WS_OVERLAPPEDWINDOW,
			300,
			300,
			wr.right - wr.left,
			wr.bottom - wr.top,
			0,
			0,
			hInstance,
			0);
	
		// TODO: another assert
		if (hWnd)
		{
			g_listBox = _createListBox(hInstance, hWnd, width, height - 85);
			g_commandBox = _createCommandBox(hInstance, hWnd, 0, height - 77, width, 17);
			g_toggleButtonBox = _createToggleButtonBox(hInstance, hWnd, width - 80, height - 18, 80, 16);

			SendMessageA(g_listBox, 48, (WPARAM)GetStockObject(12), 0);
			SendMessageA(g_commandBox, 48, (WPARAM)GetStockObject(12), 0);
			SendMessageA(g_toggleButtonBox, 48, (WPARAM)GetStockObject(12), 0);

			SetWindowTextA(g_toggleButtonBox, "StartProfiling");

			ShowWindow(hWnd, 1);
			SetFocus(hWnd);
			UpdateWindow(hWnd);

			return true;
		}

		
		return false;
	}

	void _loop(void* pArg)
	{
		MSG message;

		_createWindow(GetModuleHandle(0));

		do 
		{
			TranslateMessage(&message);
			DispatchMessageA(&message);
		} while (GetMessageA(&message, 0, 0, 0));
	}
}