#ifndef __STRINGBUILDER_H__
#define __STRINGBUILDER_H__

#include "StringBuilderBase.h"
#include "StringBuilderBaseImpl.h"

namespace fb
{
	class StringBuilder : public StringBuilderBase
	{
	public:
		StringBuilder()
			: StringBuilderBase()
		{
		}
	};
}

#endif /* __STRINGBUILDER_H__ */