#ifndef __STRINGBUILDERBASE_H__
#define __STRINGBUILDERBASE_H__

#include "StringBuilderBaseImpl.h"

namespace fb
{
	class StringBuilderBase : public StringBuilderBaseImpl
	{
	public:
		void operator<<(const char* s);
		void operator<<(char c);
		void operator<<(int value);
		void operator<<(uint value);
	};
}

#endif /* __STRINGBUILDERBASE_H__ */