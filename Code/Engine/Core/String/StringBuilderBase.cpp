#include "Main.h"
#include "StringBuilderBaseImpl.h"
#include "StringBuilderBase.h"

#include "boost/lexical_cast.hpp"

namespace fb
{
	std::string toString(int n)
	{
		using boost::lexical_cast;
		return lexical_cast<std::string>(n);
	}

	void StringBuilderBase::operator<<(const char* s)
	{
		append(s);
	}

	void StringBuilderBase::operator<<(char c)
	{
		append(toString(c));
	}

	void StringBuilderBase::operator<<(int value)
	{
		append(toString(value));
	}

	void StringBuilderBase::operator<<(uint value)
	{
		append(toString(value));
	}
}