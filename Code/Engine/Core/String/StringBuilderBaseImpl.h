#ifndef __STRINGBUILDERBASEIMPL_H__
#define __STRINGBUILDERBASEIMPL_H__

namespace fb
{
	class StringBuilderBaseImpl
	{
	public:
		int			m_capacity;
		int			m_offset;
		std::string m_string;
		
		void append(std::string data);
		bool reserve(int newCapacity);
		
	};
}

#endif /* __STRINGBUILDERBASEIMPL_H__ */