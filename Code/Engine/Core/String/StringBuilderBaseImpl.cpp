#include "Main.h"
#include "StringBuilder.h"
#include "StringBuilderBaseImpl.h"

namespace fb
{
	void StringBuilderBaseImpl::append(std::string data)
	{
		int length = data.length(),
			newsize = (m_offset + m_capacity);

		if (newsize <= length)
		{
			if (!reserve(length))
				return;
		}

		m_string += data;
		m_offset += length;
	}

	bool StringBuilderBaseImpl::reserve(int newCapacity)
	{
		m_capacity += newCapacity;
		return true;
	}
}