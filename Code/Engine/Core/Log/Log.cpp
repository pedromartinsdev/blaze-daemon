#include "Main.h"
#include "WinMainHelper.h"
#include "Buffer.h"
#include "OutStream.h"
#include "StringBuilder.h"

namespace fb
{
	namespace Log
	{
		void print(fb::Log::Level level, const char* safeFilename, int line, const char* fmt, ...)
		{
			//safeFilename += 46; //"C:\projects_pc\BlazeXpack5.Daemon.Server\Code\"

			char szVaBuffer[1024];

			va_list va;
			va_start(va, fmt);

			int len = _vsnprintf(szVaBuffer, sizeof(szVaBuffer), fmt, va);
			szVaBuffer[len] = 0;

			va_end(va);

			fb::StringBuilder builder;
			builder << safeFilename;
			builder << "(";
			builder << line;
			builder << "): ";

			switch (level)
			{
			case Info:
				builder << "Info: ";
				break;

			case Warning:
				builder << "Warning: ";
				break;

			case Error:
				builder << "Error: ";
				break;
			}

			builder << szVaBuffer;
			builder << "\n";

			fb::_logToConsole(builder);
		}
	}
}