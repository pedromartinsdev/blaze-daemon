#ifndef __LOG_H__
#define __LOG_H__

namespace fb
{
	namespace Log
	{
		enum Level
		{
			Info = 0x1,
			Warning = 0x2,
			Error = 0x3
		};

		void print(fb::Log::Level level, const char* safeFilename, int line, const char* fmt, ...);
	}
}

#define FB_LOG(level, fmt, ...) fb::Log::print(level, __FILE__, __LINE__, fmt, __VA_ARGS__)
#define FB_LOGINFO(fmt, ...) FB_LOG(fb::Log::Level::Info, fmt,  __VA_ARGS__)
#define FB_LOGERROR(fmt, ...) FB_LOG(fb::Log::Level::Error, fmt, __VA_ARGS__)
#define FB_LOGWARNING(fmt, ...) FB_LOG(fb::Log::Level::Warning, fmt, __VA_ARGS__)

#endif /* __LOG_H__ */