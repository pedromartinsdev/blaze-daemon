#include "Main.h"
#include "Buffer.h"
#include "OutStream.h"

namespace fb
{
	OutStream::OutStream()
		: m_buffer(nullptr)
	{
	}

	OutStream::OutStream(fb::Buffer* buffer)
		: m_buffer(buffer)
	{
	}

	void OutStream::initialize(fb::Buffer* buffer)
	{
		m_buffer = buffer;
	}

	fb::Buffer* OutStream::getBuffer()
	{
		return m_buffer;
	}

	int OutStream::getAvailableBytes()
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return -1;
		}

		return m_buffer->getAvailableBytes();
	}

	void OutStream::skip(int count)
	{
		m_buffer->skip(count);
	}

	void OutStream::setBuffer(fb::Buffer* buffer)
	{
		m_buffer = buffer;
	}

	void OutStream::write(void* destination, int count)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(destination, count);
	}

	/** Operators **/
	void OutStream::operator<<(int64 a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(int64));
	}

	void OutStream::operator<<(bool a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(bool));
	}

	void OutStream::operator<<(char a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(char));
	}

	void OutStream::operator<<(double a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(double));
	}

	void OutStream::operator<<(float a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(float));
	}

	void OutStream::operator<<(int a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		write(&a, sizeof(int));
	}

	void OutStream::operator<<(short a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(short));
	}

	void OutStream::operator<<(unsigned char a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(unsigned char));
	}

	void OutStream::operator<<(unsigned int a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(unsigned int));
	}

	void OutStream::operator<<(unsigned short a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, sizeof(unsigned short));
	}

	void OutStream::operator<<(std::string a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->write(&a, a.size());
	}

	void OutStream::operator<<(const char* a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		int length = strlen(a);

		for (int i = 0; i < length; i++)
			m_buffer->write(&((BYTE*)a)[i], 1);
	}
}