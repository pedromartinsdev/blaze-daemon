#ifndef __INSTREAM_H__
#define __INSTREAM_H__

namespace fb
{
	class InStream
	{
	private:
		fb::Buffer* m_buffer;

	public:
		InStream();
		InStream(fb::Buffer* buffer);

		void initialize(fb::Buffer* buffer);

		fb::Buffer* getBuffer();
		int getAvailableBytes();

		void skip(int count);
		void setBuffer(fb::Buffer* buffer);

		void read(void* destination, int count);

		/** Operators **/
		void operator>>(int64& a);
		void operator>>(bool& a);
		void operator>>(char& a);
		void operator>>(double& a);
		void operator>>(float& a);
		void operator>>(int& a);
		void operator>>(short& a);
		void operator>>(unsigned char& a);
		void operator>>(unsigned int& a);
		void operator>>(unsigned short& a);
		void operator>>(std::string& a);
	};
}

#endif /* __INSTREAM_H__ */