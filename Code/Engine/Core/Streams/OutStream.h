#ifndef __OUTSTREAM_H__
#define __OUTSTREAM_H__

namespace fb
{
	class OutStream
	{
	private:
		fb::Buffer* m_buffer;

	public:
		OutStream();
		OutStream(fb::Buffer* buffer);

		void initialize(fb::Buffer* buffer);

		fb::Buffer* getBuffer();
		int getAvailableBytes();

		void skip(int count);
		void setBuffer(fb::Buffer* buffer);

		void write(void* destination, int count);

		/** Operators **/
		void operator<<(int64 a);
		void operator<<(bool a);
		void operator<<(char a);
		void operator<<(double a);
		void operator<<(float a);
		void operator<<(int a);
		void operator<<(short a);
		void operator<<(unsigned char a);
		void operator<<(unsigned int a);
		void operator<<(unsigned short a);
		void operator<<(std::string a);
		void operator<<(const char* a);
	};
}

#endif /* __OUTSTREAM_H__ */