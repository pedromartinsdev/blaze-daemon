#include "Main.h"
#include "Buffer.h"
#include "InStream.h"

namespace fb
{
	InStream::InStream()
		: m_buffer(nullptr)
	{
	}

	InStream::InStream(fb::Buffer* buffer)
		: m_buffer(buffer)
	{
	}

	void InStream::initialize(fb::Buffer* buffer)
	{
		m_buffer = buffer;
	}

	fb::Buffer* InStream::getBuffer()
	{
		return m_buffer;
	}

	int InStream::getAvailableBytes()
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return -1;
		}

		return m_buffer->getAvailableBytes();
	}

	void InStream::skip(int count)
	{
		m_buffer->skip(count);
	}

	void InStream::setBuffer(fb::Buffer* buffer)
	{
		m_buffer = buffer;
	}

	void InStream::read(void* destination, int count)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(destination, count);
	}

	/** Operators **/
	void InStream::operator>>(int64& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(int64));
	}

	void InStream::operator>>(bool& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(bool));
	}

	void InStream::operator>>(char& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(char));
	}

	void InStream::operator>>(double& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(double));
	}

	void InStream::operator>>(float& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(float));
	}

	void InStream::operator>>(int& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(int));
	}

	void InStream::operator>>(short& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(short));
	}

	void InStream::operator>>(unsigned char& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(unsigned char));
	}

	void InStream::operator>>(unsigned int& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(unsigned int));
	}

	void InStream::operator>>(unsigned short& a)
	{
		if (!m_buffer)
		{
			//TODO: fb::Log::formattedLog
			return;
		}

		m_buffer->read(&a, sizeof(unsigned short));
	}
}