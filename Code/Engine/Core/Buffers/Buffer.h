#ifndef __BUFFER_H__
#define __BUFFER_H__

#define FB_BUFFER_MAX_SIZE 32768

namespace fb
{
	class Buffer
	{
	private:
		const int	m_caps;
		int			m_bufferSize;
		void		*m_nativeHandler;

		int			m_offset;

	public:
		char		m_buffer[FB_BUFFER_MAX_SIZE];

		Buffer(int capBits);
		Buffer(char* buffer, int length);

		int getCaps();
		void *getNativeHandler();
		int getBufferSize();
		int getAvailableBytes();

		int getPosition();
		void setPosition(int offset);
		void skip(int byteCount);

		void write(void* source, int byteCount);
		void read(void* destination, int byteCount);
	};
}

#endif /* __BUFFER_H__ */