#include "Main.h"
#include "Buffer.h"

namespace fb
{
	Buffer::Buffer(int capBits)
		: m_caps(capBits), m_bufferSize(FB_BUFFER_MAX_SIZE), m_nativeHandler(0), m_offset(0)
	{
		memset(m_buffer, 0, sizeof(m_buffer));
	}

	Buffer::Buffer(char* buffer, int length)
		: m_bufferSize(length), m_caps(0)
	{
		memcpy(m_buffer, buffer, length);

		m_offset = 0;
		m_buffer[length] = 0;
	}

	int Buffer::getCaps()
	{
		return m_caps;
	}

	void* Buffer::getNativeHandler()
	{
		return m_nativeHandler;
	}

	int Buffer::getPosition()
	{
		return m_offset;
	}

	void Buffer::setPosition(int offset)
	{
		m_offset = offset < m_bufferSize ? offset : m_offset;
	}

	void Buffer::skip(int byteCount)
	{
		m_offset += byteCount < getAvailableBytes() ? byteCount : 0;
	}

	int Buffer::getBufferSize()
	{
		if (!((m_caps >> 2) & 1) || !(m_caps & 1))
		{
			// TODO: fb::Log::formattedLog
		}

		// Get the buffer size if we don't have one already
		if (m_bufferSize < 0)
		{
			// Get current position
			int pos = getPosition();
			
			// Set position to the beginning
			setPosition(0);
			
			// Get available data to be read (AKA buffer size)
			m_bufferSize = getAvailableBytes();

			// Change position to the previous position
			setPosition(pos);
		}

		return m_bufferSize;
	}

	int Buffer::getAvailableBytes()
	{
		return m_bufferSize - getPosition();
	}

	void Buffer::write(void* source, int byteCount)
	{
		if (byteCount > 0 && getBufferSize() > byteCount)
		{
			memcpy(&m_buffer[m_offset], source, byteCount);
			m_offset += byteCount;
		}
	}

	void Buffer::read(void* destination, int byteCount)
	{
		if (byteCount > 0 && getBufferSize() > byteCount)
		{
			memcpy(destination, &m_buffer[m_offset], byteCount);
			m_offset += byteCount;
		}
	}
}