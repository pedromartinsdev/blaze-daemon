#ifndef __WINMAINHELPER_H__
#define __WINMAINHELPER_H__

#include "StringBuilder.h"

namespace fb
{
	bool _createWindow(HINSTANCE hInstance);
	void _logToConsole(fb::StringBuilder builder);
	void _loop(void* pArg);
}

#endif /* __WINMAINHELPER_H__ */