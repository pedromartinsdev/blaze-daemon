#ifndef __XMUTEX_H__
#define __XMUTEX_H__

class Xmutex
{
private:
	HANDLE m_handle;

public:
	Xmutex();
	~Xmutex();

	void lock();
	void unlock();
};

#endif /* __XMUTEX_H__ */