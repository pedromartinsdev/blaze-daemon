#include "Main.h"
#include "Xmutex.h"

Xmutex::Xmutex()
{
	m_handle = CreateMutexA(0, FALSE, 0);
}

Xmutex::~Xmutex()
{
	ReleaseMutex(m_handle);
	CloseHandle(m_handle);
}

void Xmutex::lock()
{
	WaitForSingleObject(m_handle, -1);
}

void Xmutex::unlock()
{
	ReleaseMutex(m_handle);
}