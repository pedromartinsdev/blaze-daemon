#ifndef __XSTACK_H__
#define __XSTACK_H__

template <typename T>
class Xstack
{
protected:
	std::stack<T> m_stack;

public:
	T pop()
	{
		T obj = m_stack.top();
		m_stack.pop();

		return obj;
	}

	void push(T obj)
	{
		m_stack.push(obj);
	}
};

#endif /* __XSTACK_H__ */