solution "BlazeXpack5.Daemon.Server"
	configurations 	{ "Debug", "Release" }
	flags			{ "Unicode", "Symbols" }
	includedirs		{ "Include/", "Code/", "Code/**", "Local/Build/__Generated__" }
	libdirs			{ "Libs/" }
	
	configuration "Debug*"
		targetdir "Local/Bin/"
		
	configuration "Release*"
		targetdir "Local/Build/"
	
	project "Blaze.Main_Win32_daemon"
		language "C++"
		kind "ConsoleApp"
		targetname "Blaze.Main_Win32_daemon"
		
		files { "Code/**.h", "Code/**.cpp" }
		
		postbuildcommands { "\"Local/Build/buildnumber.exe\""  }