solution "buildnumber"
	configurations 	{ "Debug", "Release" }
	flags			{ "Unicode", "Symbols" }
		
		configuration "Debug*"
		targetdir "../../../Local/Build/"
		
	configuration "Release*"
		targetdir "../../../Local/Build/"
	
	project "buildnumber"
		language "C++"
		kind "ConsoleApp"
		targetname "buildnumber"
		
		files { "src/**.h", "src/**.c" }